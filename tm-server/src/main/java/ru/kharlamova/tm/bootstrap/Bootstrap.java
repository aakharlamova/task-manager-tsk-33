package ru.kharlamova.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.kharlamova.tm.api.repository.IProjectRepository;
import ru.kharlamova.tm.api.repository.ISessionRepository;
import ru.kharlamova.tm.api.repository.ITaskRepository;
import ru.kharlamova.tm.api.repository.IUserRepository;
import ru.kharlamova.tm.api.service.*;
import ru.kharlamova.tm.component.Backup;
import ru.kharlamova.tm.endpoint.*;
import ru.kharlamova.tm.enumerated.Role;
import ru.kharlamova.tm.repository.ProjectRepository;
import ru.kharlamova.tm.repository.SessionRepository;
import ru.kharlamova.tm.repository.TaskRepository;
import ru.kharlamova.tm.repository.UserRepository;
import ru.kharlamova.tm.service.*;
import ru.kharlamova.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
@Setter
@NoArgsConstructor
public class Bootstrap implements ServiceLocator {

    @NotNull
    private final ServiceLocator serviceLocator = this;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final IDomainService domainService = new DomainService(serviceLocator);

    @NotNull
    private final Backup backup = new Backup(propertyService, domainService);

    @NotNull
    public final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    public final ISessionService sessionService = new SessionService(this, sessionRepository);

    @NotNull
    public final SessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    public final TaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    public final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    public final ProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    public final UserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    public final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

    @NotNull
    public final AdminEndpoint adminEndpoint = new AdminEndpoint(this);

    private void initData() {
        String testId = userService.create("test", "test", "test@test.ru").getId();
        projectService.add(testId, "Project1", "desc");
        taskService.add(testId, "Task11", "desc");
        taskService.add(testId, "Task12", "desc");
        taskService.add(testId, "Task13", "desc");
        String adminId = userService.create("admin", "admin", Role.ADMIN).getId();
        projectService.add(adminId, "Project2", "desc");
        taskService.add(adminId, "Task21", "desc");
        taskService.add(adminId, "Task22", "desc");
        taskService.add(adminId, "Task23", "desc");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initEndpoint() {
        registry(adminEndpoint);
        registry(adminUserEndpoint);
        registry(projectEndpoint);
        registry(projectTaskEndpoint);
        registry(sessionEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    public void run(final String... args) throws Exception {
        loggerService.debug("TEST");
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        initPID();
        initData();
        backup.load();
        backup.init();
        initEndpoint();
    }

}
