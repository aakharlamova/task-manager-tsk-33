package ru.kharlamova.tm.exception.entity;

import ru.kharlamova.tm.exception.AbstractException;

public class ObjectNotFoundException extends AbstractException {

    public ObjectNotFoundException() {
        super("Error! Object not found.");
    }

}
