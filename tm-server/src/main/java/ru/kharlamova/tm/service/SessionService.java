package ru.kharlamova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.api.repository.ISessionRepository;
import ru.kharlamova.tm.api.service.IPropertyService;
import ru.kharlamova.tm.api.service.ISessionService;
import ru.kharlamova.tm.api.service.ServiceLocator;
import ru.kharlamova.tm.enumerated.Role;
import ru.kharlamova.tm.exception.authorization.AccessDeniedException;
import ru.kharlamova.tm.model.Session;
import ru.kharlamova.tm.model.User;
import ru.kharlamova.tm.util.HashUtil;

import java.util.Optional;

import static org.reflections.util.Utils.isEmpty;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final ServiceLocator serviceLocator;

    @NotNull
    private final ISessionRepository sessionRepository;

    public SessionService(
            @NotNull final ServiceLocator serviceLocator,
            @NotNull final ISessionRepository sessionRepository) {
        super(sessionRepository);
        this.serviceLocator = serviceLocator;
        this.sessionRepository = sessionRepository;
    }

    @Nullable
    @Override
    public Session open(
            @Nullable final String login,
            @Nullable final String password
    ) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        final @NotNull Optional<User> user = serviceLocator.getUserService().findByLogin(login);
        if (!user.isPresent()) return null;
        @NotNull final Session session = new Session();
        session.setUserId(user.get().getId());
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.add(session);
        return sign(session);
    }

    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (isEmpty(login) || isEmpty(password)) return false;
        final @NotNull Optional<User> user = serviceLocator.getUserService().findByLogin(login);
        if (!user.isPresent()) return false;
        final String passwordHash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        if (isEmpty(passwordHash)) return false;
        return passwordHash.equals(user.get().getPasswordHash());
    }

    @Override
    public void validate(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        if (isEmpty(session.getSignature())) throw new AccessDeniedException();
        if (isEmpty(session.getUserId())) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final Session sessionTarget = sign(temp);
        if (sessionTarget == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionTarget.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (!sessionRepository.contains(session.getId())) throw new AccessDeniedException();
    }

    @Override
    public void validateAdmin(@Nullable final Session session, @Nullable final Role role) {
        if (session == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        if (isEmpty(session.getUserId())) throw new AccessDeniedException();
        validate(session);
        final @NotNull Optional<User> user = serviceLocator.getUserService().findById(session.getUserId());
        if (!user.isPresent()) throw new AccessDeniedException();
        if (user.get().getRole() != Role.ADMIN) throw new AccessDeniedException();
    }

    @Nullable
    @SneakyThrows
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @Nullable final String signature = HashUtil.salt(propertyService, session);
        session.setSignature(signature);
        return session;
    }

    @Override
    @Nullable
    public Session close(@Nullable final Session session) {
        if (session == null) return null;
        return sessionRepository.removeById(session.getId());
    }

}
