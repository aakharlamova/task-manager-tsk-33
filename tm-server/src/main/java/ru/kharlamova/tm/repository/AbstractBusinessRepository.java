package ru.kharlamova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.api.IBusinessRepository;
import ru.kharlamova.tm.exception.entity.ObjectNotFoundException;
import ru.kharlamova.tm.model.AbstractBusinessEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.reflections.util.Utils.isEmpty;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> implements IBusinessRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @NotNull
    public Predicate<E> predicateById(@NotNull final String id) {
        return e -> id.equals(e.getId());
    }

    @NotNull
    public Predicate<E> predicateByName(@NotNull final String name) {
        return e -> name.equals(e.getName());
    }

    @NotNull
    public Predicate<E> predicateByUserId(@NotNull final String userId) {
        return e -> userId.equals(e.getUserId());
    }

    @NotNull
    @Override
    public List<E> findAll() {
        @NotNull final List<E> result = new ArrayList<>();
        for (@NotNull final E entity : entities) {
            if (entity.equals(entity.getId())) result.add(entity);
        }
        return entities;
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId) {
        if (isEmpty(userId)) return new ArrayList<>();
        return entities.stream()
                .filter(predicateByUserId(userId))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId, @NotNull Comparator<E> comparator) {
        if (isEmpty(userId)) return new ArrayList<>();
        return entities.stream()
                .filter(predicateByUserId(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public E add(@NotNull final String userId, @NotNull final E entity) {
        entity.setUserId(userId);
        entities.add(entity);
        return entity;
    }

    @NotNull
    @Override
    public Optional<E> findById(@Nullable final String userId, @NotNull final String id) {
        if (isEmpty(userId)) return Optional.empty();
        return entities.stream()
                .filter(predicateByUserId(userId))
                .filter(predicateById(id))
                .findFirst();
    }

    @NotNull
    @Override
    public Optional<E> findByIndex(@Nullable final String userId, @NotNull final Integer index) {
        if (isEmpty(userId)) return Optional.empty();
        return entities.stream()
                .filter(predicateByUserId(userId))
                .skip(index)
                .findFirst();
    }

    @NotNull
    @Override
    public Optional<E> findByName(@Nullable final String userId, @NotNull final String name) {
        if (isEmpty(userId)) return Optional.empty();
        return entities.stream()
                .filter(predicateByUserId(userId))
                .filter(predicateByName(name))
                .limit(1)
                .findFirst();
    }

    @Override
    public void clear(@NotNull final String userId) {
        entities.stream().filter(predicateByUserId(userId))
                .collect(Collectors.toList())
                .forEach(entities::remove);
    }

    @Nullable
    @Override
    public E removeById(@Nullable final String userId, @NotNull final String id) {
        final Optional<E> entity = findById(userId, id);
        entity.ifPresent(this::remove);
        entity.orElseThrow(ObjectNotFoundException::new);
        return entity.orElse(null);
    }

    @Nullable
    @Override
    public E removeByIndex(@Nullable final String userId, @NotNull final Integer index) {
        final Optional<E> entity = findByIndex(userId, index);
        entity.ifPresent(this::remove);
        entity.orElseThrow(ObjectNotFoundException::new);
        return entity.orElse(null);
    }

    @Nullable
    @Override
    public E removeByName(@Nullable final String userId, @NotNull final String name) {
        final Optional<E> entity = findByName(userId, name);
        entity.ifPresent(this::remove);
        entity.orElseThrow(ObjectNotFoundException::new);
        return entity.orElse(null);
    }

    @Override
    public void remove(@Nullable final String userId, @NotNull final E entity) {
        entities.remove(entity.getId());
    }

}
