package ru.kharlamova.tm.repository;

import ru.kharlamova.tm.api.repository.ISessionRepository;
import ru.kharlamova.tm.model.Session;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {
}
