package ru.kharlamova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.api.service.ServiceLocator;
import ru.kharlamova.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class SessionEndpoint extends AbstractEndpoint {

    public SessionEndpoint() {
        super(null);
    }

    public SessionEndpoint(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    public Session openSession(
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "password", partName = "password") final String password
    ) {
        return serviceLocator.getSessionService().open(login, password);
    }

    @WebMethod
    public List<Session> listSession() {
        return serviceLocator.getSessionService().findAll();
    }

    @Nullable
    @WebMethod
    public Session closeSession(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getSessionService().close(session);
    }

}
