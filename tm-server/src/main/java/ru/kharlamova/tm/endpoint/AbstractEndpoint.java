package ru.kharlamova.tm.endpoint;

import ru.kharlamova.tm.api.service.ServiceLocator;

public abstract class AbstractEndpoint {

    final ServiceLocator serviceLocator;

    public AbstractEndpoint(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
