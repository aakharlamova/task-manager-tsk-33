package ru.kharlamova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.api.service.ServiceLocator;
import ru.kharlamova.tm.model.Project;
import ru.kharlamova.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class ProjectEndpoint extends AbstractEndpoint {

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @WebMethod
    public Project addProject(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().add(session.getUserId(), name, description);
    }

    @WebMethod
    public List<Project> findProjectAllWithComparator(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "sort", partName = "sort") @NotNull final String sort
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId(), sort);
    }

    @NotNull
    @WebMethod
    public List<Project> findProjectAll(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @Nullable
    @WebMethod
    public Project findProjectById(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findById(session.getUserId(), id).orElse(null);
    }

    @Nullable
    @WebMethod
    public Project findProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findByIndex(session.getUserId(), index).orElse(null);
    }

    @Nullable
    @WebMethod
    public Project findProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findByName(session.getUserId(), name).orElse(null);
    }

    @WebMethod
    public void removeProject(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "project", partName = "project") @NotNull Project project
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().remove(session.getUserId(), project);
    }

    @WebMethod
    public Project removeProjectById(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeById(session.getUserId(), id);
    }

    @WebMethod
    public Project removeProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeByIndex(session.getUserId(), index);
    }

    @WebMethod
    public Project removeProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeByName(session.getUserId(), name);
    }

}
