package ru.kharlamova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.api.service.ServiceLocator;
import ru.kharlamova.tm.enumerated.Status;
import ru.kharlamova.tm.model.Session;
import ru.kharlamova.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class TaskEndpoint extends AbstractEndpoint {

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @WebMethod
    public Task addTask(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().add(session.getUserId(), name, description);
    }

    @WebMethod
    @NotNull
    public List<Task> findTaskAllWithComparator(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "sort", partName = "sort") @NotNull final String sort
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(session.getUserId(), sort);
    }

    @NotNull
    @WebMethod
    public List<Task> findAllTask(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @Nullable
    @WebMethod
    public Task findTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findById(session.getUserId(), id).orElse(null);
    }

    @Nullable
    @WebMethod
    public Task findTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByIndex(session.getUserId(), index).orElse(null);
    }

    @Nullable
    @WebMethod
    public Task findTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByName(session.getUserId(), name).orElse(null);
    }

    @WebMethod
    public void removeTask(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "task", partName = "task") @NotNull Task task
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().remove(session.getUserId(), task);
    }

    @WebMethod
    public Task removeTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeById(session.getUserId(), id);
    }

    @WebMethod
    public Task removeTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeByIndex(session.getUserId(), index);
    }

    @WebMethod
    public Task removeTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeByName(session.getUserId(), name);
    }

    @Nullable
    @WebMethod
    public Task changeTaskStatusById(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().changeStatusById(session.getUserId(), id, status)
                .orElse(null);
    }

    @Nullable
    @WebMethod
    public Task changeTaskStatusByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().changeStatusByIndex(session.getUserId(), index, status)
                .orElse(null);
    }

    @Nullable
    @WebMethod
    public Task changeTaskStatusByName(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().changeStatusByName(session.getUserId(), name, status)
                .orElse(null);
    }

    @Nullable
    @WebMethod
    public Task finishTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        System.out.println(serviceLocator.getTaskService().finishById(session.getUserId(), id));
        return serviceLocator.getTaskService().finishById(session.getUserId(), id)
                .orElse(null);
    }

    @Nullable
    @WebMethod
    public Task finishTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().finishByIndex(session.getUserId(), index)
                .orElse(null);
    }

    @Nullable
    @WebMethod
    public Task finishTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().finishByName(session.getUserId(), name)
                .orElse(null);
    }

    @Nullable
    @WebMethod
    public Task startTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().startById(session.getUserId(), id)
                .orElse(null);
    }

    @Nullable
    @WebMethod
    public Task startTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().startByIndex(session.getUserId(), index)
                .orElse(null);
    }

    @Nullable
    @WebMethod
    public Task startTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().startByName(session.getUserId(), name)
                .orElse(null);
    }

    @Nullable
    @WebMethod
    public Task updateTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateById(session.getUserId(), id, name, description)
                .orElse(null);
    }

    @Nullable
    @WebMethod
    public Task updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateByIndex(session.getUserId(), index, name, description)
                .orElse(null);
    }

    @Nullable
    @WebMethod
    public Task bindTaskByProject(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().bindTaskByProject(session.getUserId(), projectId, taskId)
                .orElse(null);
    }

    @NotNull
    @WebMethod
    public List<Task> findAllByProjectId(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().findAllTaskByProjectId(session.getUserId(), projectId);
    }

    @Nullable
    @WebMethod
    public Task unbindTaskFromProject(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().unbindTaskFromProject(session.getUserId(), taskId)
                .orElse(null);
    }

}
