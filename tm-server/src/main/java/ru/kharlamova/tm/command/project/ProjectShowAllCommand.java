package ru.kharlamova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.api.service.IProjectService;
import ru.kharlamova.tm.command.AbstractProjectCommand;
import ru.kharlamova.tm.enumerated.Sort;
import ru.kharlamova.tm.exception.entity.ObjectNotFoundException;
import ru.kharlamova.tm.model.Project;
import ru.kharlamova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

import static org.reflections.util.Utils.isEmpty;

public class ProjectShowAllCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new ObjectNotFoundException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sort = TerminalUtil.nextLine();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @Nullable List<Project> projects;
        if (isEmpty(sort)) projects = projectService.findAll();
        else projects = projectService.findAll(userId, sort);
        if (projects.isEmpty()) {
            System.out.println("[NO PROJECTS]");
            return;
        }
        int index = 1;
        for (@NotNull final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println();
    }

}
