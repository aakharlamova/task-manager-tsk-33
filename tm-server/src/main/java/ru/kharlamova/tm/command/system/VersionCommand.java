package ru.kharlamova.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.command.AbstractCommand;
import ru.kharlamova.tm.exception.entity.ObjectNotFoundException;

public class VersionCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String description() {
        return "Show application version.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new ObjectNotFoundException();
        System.out.println("[VERSION]");
        @Nullable final String version;
        if (Manifests.exists("version")) version = Manifests.read("version");
        else version = serviceLocator.getPropertyService().getApplicationVersion();
        System.out.println(version);
    }

}
