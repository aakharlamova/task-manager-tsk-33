package ru.kharlamova.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import ru.kharlamova.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}
