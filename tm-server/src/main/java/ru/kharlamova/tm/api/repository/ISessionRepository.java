package ru.kharlamova.tm.api.repository;

import ru.kharlamova.tm.api.IRepository;
import ru.kharlamova.tm.model.Session;

public interface ISessionRepository extends IRepository<Session> {
}
