package ru.kharlamova.tm;

import ru.kharlamova.tm.bootstrap.Bootstrap;
import ru.kharlamova.tm.util.SystemUtil;

public class Application {

    public static void main(String[] args) throws Exception {
        System.out.println(SystemUtil.getPID());
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}