package ru.kharlamova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.command.AbstractProjectCommand;
import ru.kharlamova.tm.endpoint.Project;
import ru.kharlamova.tm.endpoint.Session;
import ru.kharlamova.tm.exception.entity.ProjectNotFoundException;
import ru.kharlamova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectByIndexStartCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-start-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Change project status to In progress by project index.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("[CHANGE PROJECT]");
        System.out.println("[ENTER INDEX:]");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Optional<Project> project = endpointLocator.getProjectEndpoint().startByIndex(session, index);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
    }

}
