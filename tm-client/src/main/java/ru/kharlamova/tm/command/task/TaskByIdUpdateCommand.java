package ru.kharlamova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.command.AbstractTaskCommand;
import ru.kharlamova.tm.endpoint.Session;
import ru.kharlamova.tm.endpoint.Task;
import ru.kharlamova.tm.exception.entity.TaskNotFoundException;
import ru.kharlamova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskByIdUpdateCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-update-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Update a task by id.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Task task = endpointLocator.getTaskEndpoint().findTaskById(session, id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        System.out.println("[ENTER NAME:]");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final Task taskUpdate = endpointLocator.getTaskEndpoint().updateTaskById(session, id, name, description);
        Optional.ofNullable(taskUpdate).orElseThrow(TaskNotFoundException::new);
    }

}
