package ru.kharlamova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.command.AbstractTaskCommand;
import ru.kharlamova.tm.endpoint.Session;
import ru.kharlamova.tm.endpoint.Status;
import ru.kharlamova.tm.endpoint.Task;
import ru.kharlamova.tm.exception.entity.TaskNotFoundException;
import ru.kharlamova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public class TaskByIndexChangeStatusCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-change-status-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Change task status by task index.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("[CHANGE TASK]");
        System.out.println("[ENTER INDEX:]");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("[ENTER STATUS:]");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusId = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusId);
        @NotNull final Task task = endpointLocator.getTaskEndpoint().findTaskByIndex(session, index);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        @NotNull final Task taskStatusUpdate = endpointLocator.getTaskEndpoint().changeTaskStatusByIndex(session, index, status);
        Optional.ofNullable(taskStatusUpdate).orElseThrow(TaskNotFoundException::new);
    }

}
