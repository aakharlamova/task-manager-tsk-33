package ru.kharlamova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.command.AbstractProjectCommand;
import ru.kharlamova.tm.endpoint.Project;
import ru.kharlamova.tm.endpoint.Session;
import ru.kharlamova.tm.exception.entity.ProjectNotFoundException;
import ru.kharlamova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectByIndexRemoveCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear a project by index.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("[ENTER INDEX:]");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Project project = endpointLocator.getProjectEndpoint().removeProjectByIndex(session, index);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
    }

}
