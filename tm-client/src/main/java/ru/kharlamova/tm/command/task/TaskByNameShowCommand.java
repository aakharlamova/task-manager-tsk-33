package ru.kharlamova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.command.AbstractTaskCommand;
import ru.kharlamova.tm.endpoint.Session;
import ru.kharlamova.tm.endpoint.Task;
import ru.kharlamova.tm.exception.entity.TaskNotFoundException;
import ru.kharlamova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskByNameShowCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-view-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Show a task by name.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER NAME:]");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final Task task = endpointLocator.getTaskEndpoint().findTaskByName(session, name);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        showTask(task);
    }

}
