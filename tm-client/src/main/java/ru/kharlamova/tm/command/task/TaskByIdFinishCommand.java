package ru.kharlamova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.command.AbstractTaskCommand;
import ru.kharlamova.tm.endpoint.Session;
import ru.kharlamova.tm.endpoint.Task;
import ru.kharlamova.tm.exception.entity.TaskNotFoundException;
import ru.kharlamova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskByIdFinishCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-finish-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Change task status to Complete by task id.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("[FINISH TASK]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Task task = endpointLocator.getTaskEndpoint().finishTaskById(session, id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
    }

}
