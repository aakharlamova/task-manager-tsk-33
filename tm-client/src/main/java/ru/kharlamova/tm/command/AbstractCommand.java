package ru.kharlamova.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.api.service.EndpointLocator;
import ru.kharlamova.tm.endpoint.*;

public abstract class AbstractCommand {

    @Nullable
    protected EndpointLocator endpointLocator;

    public void setEndpointLocator(@NotNull EndpointLocator endpointLocator) {
        this.endpointLocator = endpointLocator;
    }

    @Nullable
    public Role[] roles() {
        return null;
    }

    @Nullable
    public abstract String arg();

    @NotNull
    public abstract String name();

    @Nullable
    public abstract String description();

    public abstract void execute();

}
