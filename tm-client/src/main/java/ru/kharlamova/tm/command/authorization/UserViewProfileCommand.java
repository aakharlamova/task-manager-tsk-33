package ru.kharlamova.tm.command.authorization;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.command.AbstractCommand;
import ru.kharlamova.tm.endpoint.Session;
import ru.kharlamova.tm.endpoint.User;

public class UserViewProfileCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "show-profile";
    }

    @NotNull
    @Override
    public String description() {
        return "Show  profile information.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        @NotNull final User user = endpointLocator.getUserEndpoint().findUserById(session);
        System.out.println("[VIEW PROFILE]");
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("[OK]");
    }

}
