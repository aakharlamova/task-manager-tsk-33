package ru.kharlamova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.command.AbstractProjectCommand;
import ru.kharlamova.tm.endpoint.Project;
import ru.kharlamova.tm.endpoint.ProjectEndpoint;
import ru.kharlamova.tm.endpoint.Session;
import ru.kharlamova.tm.endpoint.Status;
import ru.kharlamova.tm.exception.entity.ProjectNotFoundException;
import ru.kharlamova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public class ProjectByIdChangeStatusCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-change-status-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Change project status by project id.";
    }

    @Override
    public void execute() {
        @Nullable final Session session = endpointLocator.getSession();
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusId = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusId);
        @Nullable final Project projectStatusUpdate = projectEndpoint.changeProjectStatusById(session, id, status);
        Optional.ofNullable(projectStatusUpdate).orElseThrow(ProjectNotFoundException::new);
    }

}
