package ru.kharlamova.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-07-12T10:57:29.697+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.kharlamova.ru/", name = "ProjectEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ProjectEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.kharlamova.ru/ProjectEndpoint/findProjectAllWithComparatorRequest", output = "http://endpoint.tm.kharlamova.ru/ProjectEndpoint/findProjectAllWithComparatorResponse")
    @RequestWrapper(localName = "findProjectAllWithComparator", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.FindProjectAllWithComparator")
    @ResponseWrapper(localName = "findProjectAllWithComparatorResponse", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.FindProjectAllWithComparatorResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.kharlamova.tm.endpoint.Project> findProjectAllWithComparator(
        @WebParam(name = "session", targetNamespace = "")
        ru.kharlamova.tm.endpoint.Session session,
        @WebParam(name = "sort", targetNamespace = "")
        java.lang.String sort
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kharlamova.ru/ProjectEndpoint/findProjectByIdRequest", output = "http://endpoint.tm.kharlamova.ru/ProjectEndpoint/findProjectByIdResponse")
    @RequestWrapper(localName = "findProjectById", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.FindProjectById")
    @ResponseWrapper(localName = "findProjectByIdResponse", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.FindProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.kharlamova.tm.endpoint.Project findProjectById(
        @WebParam(name = "session", targetNamespace = "")
        ru.kharlamova.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kharlamova.ru/ProjectEndpoint/findProjectByNameRequest", output = "http://endpoint.tm.kharlamova.ru/ProjectEndpoint/findProjectByNameResponse")
    @RequestWrapper(localName = "findProjectByName", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.FindProjectByName")
    @ResponseWrapper(localName = "findProjectByNameResponse", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.FindProjectByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.kharlamova.tm.endpoint.Project findProjectByName(
        @WebParam(name = "session", targetNamespace = "")
        ru.kharlamova.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kharlamova.ru/ProjectEndpoint/removeProjectByIndexRequest", output = "http://endpoint.tm.kharlamova.ru/ProjectEndpoint/removeProjectByIndexResponse")
    @RequestWrapper(localName = "removeProjectByIndex", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.RemoveProjectByIndex")
    @ResponseWrapper(localName = "removeProjectByIndexResponse", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.RemoveProjectByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.kharlamova.tm.endpoint.Project removeProjectByIndex(
        @WebParam(name = "session", targetNamespace = "")
        ru.kharlamova.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kharlamova.ru/ProjectEndpoint/addProjectRequest", output = "http://endpoint.tm.kharlamova.ru/ProjectEndpoint/addProjectResponse")
    @RequestWrapper(localName = "addProject", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.AddProject")
    @ResponseWrapper(localName = "addProjectResponse", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.AddProjectResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.kharlamova.tm.endpoint.Project addProject(
        @WebParam(name = "session", targetNamespace = "")
        ru.kharlamova.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kharlamova.ru/ProjectEndpoint/removeProjectRequest", output = "http://endpoint.tm.kharlamova.ru/ProjectEndpoint/removeProjectResponse")
    @RequestWrapper(localName = "removeProject", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.RemoveProject")
    @ResponseWrapper(localName = "removeProjectResponse", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.RemoveProjectResponse")
    public void removeProject(
        @WebParam(name = "session", targetNamespace = "")
        ru.kharlamova.tm.endpoint.Session session,
        @WebParam(name = "project", targetNamespace = "")
        ru.kharlamova.tm.endpoint.Project project
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kharlamova.ru/ProjectEndpoint/removeProjectByIdRequest", output = "http://endpoint.tm.kharlamova.ru/ProjectEndpoint/removeProjectByIdResponse")
    @RequestWrapper(localName = "removeProjectById", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.RemoveProjectById")
    @ResponseWrapper(localName = "removeProjectByIdResponse", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.RemoveProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.kharlamova.tm.endpoint.Project removeProjectById(
        @WebParam(name = "session", targetNamespace = "")
        ru.kharlamova.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kharlamova.ru/ProjectEndpoint/findProjectAllRequest", output = "http://endpoint.tm.kharlamova.ru/ProjectEndpoint/findProjectAllResponse")
    @RequestWrapper(localName = "findProjectAll", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.FindProjectAll")
    @ResponseWrapper(localName = "findProjectAllResponse", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.FindProjectAllResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.kharlamova.tm.endpoint.Project> findProjectAll(
        @WebParam(name = "session", targetNamespace = "")
        ru.kharlamova.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kharlamova.ru/ProjectEndpoint/findProjectByIndexRequest", output = "http://endpoint.tm.kharlamova.ru/ProjectEndpoint/findProjectByIndexResponse")
    @RequestWrapper(localName = "findProjectByIndex", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.FindProjectByIndex")
    @ResponseWrapper(localName = "findProjectByIndexResponse", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.FindProjectByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.kharlamova.tm.endpoint.Project findProjectByIndex(
        @WebParam(name = "session", targetNamespace = "")
        ru.kharlamova.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kharlamova.ru/ProjectEndpoint/removeProjectByNameRequest", output = "http://endpoint.tm.kharlamova.ru/ProjectEndpoint/removeProjectByNameResponse")
    @RequestWrapper(localName = "removeProjectByName", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.RemoveProjectByName")
    @ResponseWrapper(localName = "removeProjectByNameResponse", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.RemoveProjectByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.kharlamova.tm.endpoint.Project removeProjectByName(
        @WebParam(name = "session", targetNamespace = "")
        ru.kharlamova.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );
}
