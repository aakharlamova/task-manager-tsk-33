package ru.kharlamova.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-07-12T10:57:31.108+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.kharlamova.ru/", name = "UserEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface UserEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.kharlamova.ru/UserEndpoint/setPasswordRequest", output = "http://endpoint.tm.kharlamova.ru/UserEndpoint/setPasswordResponse")
    @RequestWrapper(localName = "setPassword", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.SetPassword")
    @ResponseWrapper(localName = "setPasswordResponse", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.SetPasswordResponse")
    public void setPassword(
        @WebParam(name = "session", targetNamespace = "")
        ru.kharlamova.tm.endpoint.Session session,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kharlamova.ru/UserEndpoint/findUserByIdRequest", output = "http://endpoint.tm.kharlamova.ru/UserEndpoint/findUserByIdResponse")
    @RequestWrapper(localName = "findUserById", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.FindUserById")
    @ResponseWrapper(localName = "findUserByIdResponse", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.FindUserByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.kharlamova.tm.endpoint.User findUserById(
        @WebParam(name = "session", targetNamespace = "")
        ru.kharlamova.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kharlamova.ru/UserEndpoint/createUserRequest", output = "http://endpoint.tm.kharlamova.ru/UserEndpoint/createUserResponse")
    @RequestWrapper(localName = "createUser", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.CreateUser")
    @ResponseWrapper(localName = "createUserResponse", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.CreateUserResponse")
    public void createUser(
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password,
        @WebParam(name = "email", targetNamespace = "")
        java.lang.String email
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kharlamova.ru/UserEndpoint/userUpdateRequest", output = "http://endpoint.tm.kharlamova.ru/UserEndpoint/userUpdateResponse")
    @RequestWrapper(localName = "userUpdate", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.UserUpdate")
    @ResponseWrapper(localName = "userUpdateResponse", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.UserUpdateResponse")
    public void userUpdate(
        @WebParam(name = "session", targetNamespace = "")
        ru.kharlamova.tm.endpoint.Session session,
        @WebParam(name = "firstName", targetNamespace = "")
        java.lang.String firstName,
        @WebParam(name = "lastName", targetNamespace = "")
        java.lang.String lastName,
        @WebParam(name = "middleName", targetNamespace = "")
        java.lang.String middleName
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kharlamova.ru/UserEndpoint/findUserByLoginRequest", output = "http://endpoint.tm.kharlamova.ru/UserEndpoint/findUserByLoginResponse")
    @RequestWrapper(localName = "findUserByLogin", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.FindUserByLogin")
    @ResponseWrapper(localName = "findUserByLoginResponse", targetNamespace = "http://endpoint.tm.kharlamova.ru/", className = "ru.kharlamova.tm.endpoint.FindUserByLoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.kharlamova.tm.endpoint.User findUserByLogin(
        @WebParam(name = "session", targetNamespace = "")
        ru.kharlamova.tm.endpoint.Session session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login
    );
}
