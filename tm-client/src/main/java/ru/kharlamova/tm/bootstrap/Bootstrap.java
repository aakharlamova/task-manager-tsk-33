package ru.kharlamova.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.kharlamova.tm.api.repository.*;
import ru.kharlamova.tm.api.service.*;
import ru.kharlamova.tm.command.AbstractCommand;
import ru.kharlamova.tm.component.FileScanner;
import ru.kharlamova.tm.endpoint.*;
import ru.kharlamova.tm.exception.system.UnknownArgumentException;
import ru.kharlamova.tm.exception.system.UnknownCommandException;
import ru.kharlamova.tm.repository.*;
import ru.kharlamova.tm.service.*;
import ru.kharlamova.tm.util.SystemUtil;
import ru.kharlamova.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class Bootstrap implements EndpointLocator {

    @NotNull  private final IPropertyService propertyService = new PropertyService();

    @NotNull private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull  private final ILoggerService loggerService = new LoggerService();

    @NotNull private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull private final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @NotNull private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @NotNull private final FileScanner fileScanner = new FileScanner(this);

    @Nullable
    private Session session;

    @SneakyThrows
    private void init() {
        @NotNull final Reflections reflections = new Reflections("ru.kharlamova.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.kharlamova.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void run(final String... args) throws Exception {
        loggerService.debug("TEST");
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        initPID();
        init();
        fileScanner.init();
        //noinspection InfiniteLoopStatement
        while (true) {
            System.out.println("ENTER COMMAND:");
            @NotNull final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public void registry(final AbstractCommand command) {
        if (command == null) return;
        command.setEndpointLocator(this);
        commandService.add(command);
    }

    public void parseCommand(final String cmd) {
        if (!Optional.ofNullable(cmd).isPresent()) return;
        @Nullable final AbstractCommand command= commandService.getCommandByName(cmd);
        if (!Optional.ofNullable(command).isPresent()) throw new UnknownCommandException(cmd);
        @Nullable final Role[] roles = command.roles();
        command.execute();
    }

    public void parseArg(String arg) {
        if (!Optional.ofNullable(arg).isPresent()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        if (!Optional.ofNullable(command).isPresent()) throw new UnknownArgumentException(arg);
        command.execute();
    }

    public boolean parseArgs(String[] args) throws Exception {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0) return false;
        @Nullable final String arg = args[0];
        parseArg(arg);
        return true;
    }

    @Override
    public @NotNull AdminUserEndpoint getAdminUserEndpoint() {
        return null;
    }

}
